<p align="center" width="100%">
  <a href="https://bonloyalty.com" target="_blank"><img width="33%" src="https://bonloyalty.com/wp-content/uploads/2022/04/bon-loyalty.svg" alt="BON Loyalty is a trusted rewards and referrals solution provider that helps merchants increase customer engagement with captivating points, rewards & referral program."></a>
</p>

# BON Loyalty Headless Store Widget Component

## Getting started

Install [BON Loyalty app](https://apps.shopify.com/bon-loyalty-rewards?utm=headless-component) on your Shopify Store and enable Widget on your Storefront and make sure Widget loaded correctly on Storefront!

## Import BON Loyalty Widget to use on your headless store
```
import BonLoyaltyWidget from '@bon-loyalty/bon-loyalty-shopify-hydrogen-widget';
```
On the app return template
```
<BonLoyaltyWidget />
```


![BON Loyalty Storefront Widget](https://gitlab.com/bon-loyalty/js-sdk/-/raw/main/images/BON-Loyalty-storefront-widget.png?ref_type=heads "BON Loyalty Storefront Widget")



## Hydrogen template: Skeleton

Hydrogen is Shopify’s stack for headless commerce. Hydrogen is designed to dovetail with [Remix](https://remix.run/), Shopify’s full stack web framework. This template contains a **minimal setup** of components, queries and tooling to get started with Hydrogen.

[Check out Hydrogen docs](https://shopify.dev/custom-storefronts/hydrogen)
[Get familiar with Remix](https://remix.run/docs/en/v1)

### What's included

- Remix
- Hydrogen
- Oxygen
- Shopify CLI
- ESLint
- Prettier
- GraphQL generator
- TypeScript and JavaScript flavors
- Minimal setup of components and routes

### Getting started

**Requirements:**

- Node.js version 16.14.0 or higher

```bash
npm create @shopify/hydrogen@latest
```

### Building for production

```bash
npm run build
```

### Local development

```bash
npm run dev
```
