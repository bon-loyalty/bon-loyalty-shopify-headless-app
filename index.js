import BonLoyaltyWidget from "./src/components/BonLoyaltyWidget";
import LoyaltyPage from "./src/components/loyaltyPage";
import LoyaltyPageHeader from "./src/components/loyaltyPage/LoyaltyPageHeader";
import LoyaltyPageEarning from "./src/components/loyaltyPage/LoyaltyPageEarning";
import LoyaltyPageRedeem from "./src/components/loyaltyPage/LoyaltyPageRedeem";
import LoyaltyPageReferral from "./src/components/loyaltyPage/LoyaltyPageReferral";
import LoyaltyPageTier from "./src/components/loyaltyPage/LoyaltyPageTier";
import LoyaltyPageProfile from "./src/components/loyaltyPage/LoyaltyPageProfile";

export {
    BonLoyaltyWidget,
    LoyaltyPage,
    LoyaltyPageHeader,
    LoyaltyPageEarning,
    LoyaltyPageRedeem,
    LoyaltyPageReferral,
    LoyaltyPageTier,
    LoyaltyPageProfile
}
