/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react'
import { BASE_URL, SHOP_DATA, LoadingSpinner, EARNING } from '../BonLoyaltyWidget'
import LoginRequired from './popup/LoginRequired';

const LoyaltyPageEarning = ({ shopUrl, lang, isAuth, customId }) => {
    const [isLoading, setLoading] = useState(true);
    const [isSignIn, setSignIn] = useState(false);
    const [shopData, setShopData] = useState({})
    const [listEarnPoint, setListEarnPoint] = useState([]);
    useEffect(() => {
        // getShopData()
        if (isAuth) {
            getListEarnPointAfterLogin()
        } else {
            getListEarnPointBeforeLogin()
        }
    }, [])
    const getShopData = async () => {
        setLoading(true)
        try {
            const response = await fetch(`${BASE_URL}${SHOP_DATA.GET_ALL}?shop=${shopUrl}&lang=${lang}`);
            const json = await response.json();
            setLoading(false)
            console.log(json)
            setShopData(json.data)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getShopData errrrrrrr')
        }
    }
    const getListEarnPointBeforeLogin = async () => {
        try {
            setLoading(true)
            const response = await fetch(
                `${BASE_URL}${EARNING.GET_BEFORE_LOGIN}?shop=${shopUrl}`,
            );
            const json = await response.json();
            setListEarnPoint(json.data)
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getListEarnPointBeforeLogin error')
        }
    }
    const getListEarnPointAfterLogin = async () => {
        try {
            setLoading(true)
            const response = await fetch(
                `${BASE_URL}${EARNING.GET_AFTER_LOGIN}?shop=${shopUrl}&cid=${customId}`
            );
            const json = await response.json();
            setListEarnPoint(json.data)
            setLoading(false)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getListEarnPointAfterLogin error')
        }
    }
    const renderTextAction = (type) => {
        switch (type) {
            //Create an account
            case 0:
            case 2:
            case 3:
            case 4:
            case 13:
                return 'Do it now'
            //Complete an order
            case 1:
                return 'Purchase'
            // Happy birthday
            case 5:
                return 'Enter info'
            // Leave a review
            case 6:
                return 'Purchase to review'
            // Follow on 
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                return 'Take me there'
            default:
                return 'Do it now'
        }
    }
    return (
        <div style={{ padding: '0px 50px' }}>
            <div style={{ opacity: 1.075, backgroundColor: '#ED66B20F' }} >
                <div >
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <div style={{ width: '70%' }}>
                            <h1
                                style={{ color: '#202528', fontWeight: '600', fontSize: '30px', lineHeight: 1.2, textAlign: 'center', marginBottom: 0 }}
                            >Earn points</h1>
                            <p
                                className="caption-size"
                                style={{ textAlign: 'center', color: '#828282', lineHeight: 1.5, fontWeight: '400', fontSize: '20px' }}
                            >
                                You can earn points for every dollar you spend on our website. Earn even more points by completing the actions below.
                            </p>
                        </div>
                    </div>
                    <div style={{ padding: '35px 50px', position: 'relative' }} >
                        {isLoading ? <LoadingSpinner color={'#ed66b2'} /> :
                            <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
                                {listEarnPoint.length > 0 && listEarnPoint.map((item) => (
                                    <div key={item?.id} style={{ margin: '0 20px 24px 0', minWidth: '292px' }}>
                                        <div style={{ height: "100%", borderRadius: '10px', border: '2px solid #86469c0f', backgroundColor: '#fff' }}>
                                            <div style={{ height: "100%", display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '24px 0' }} >
                                                <div style={{ height: '36px', width: '36px' }}>
                                                    <svg viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fillRule="evenodd" clipRule="evenodd" d="M0.000191711 18.0835C-0.0448926 8.1927 7.8674 0.0680674 17.8385 0.00042478C27.9149 -0.0672178 35.9549 7.95219 36 18.0309C35.9249 27.9594 28.0802 35.7458 18.4772 35.9938C8.2957 36.2569 0.045276 28.1097 0.000191711 18.0835ZM6.68018 28.6734C12.0227 22.4953 22.9105 21.6461 29.3576 28.6283C23.8348 34.8439 13.1573 35.5955 6.68018 28.6734ZM5.12477 26.7193C8.69394 23.2018 12.977 21.3229 17.9963 21.3154C23.0308 21.3154 27.3213 23.2018 30.9055 26.7268C34.9781 20.9095 34.6625 12.0709 28.501 6.45654C22.3695 0.872263 12.9018 1.06768 7.07091 6.86991C1.14233 12.7623 1.30764 21.3905 5.12477 26.7193ZM10.8054 11.928C10.8279 7.95216 14.0815 4.77295 18.1015 4.78799C22.0389 4.81053 25.2399 8.09496 25.2023 12.0859C25.1723 16.0091 21.9112 19.2109 17.9813 19.1883C13.9914 19.1658 10.7753 15.919 10.8054 11.928ZM13.2099 11.9506C13.2024 14.6187 15.3364 16.7833 17.9963 16.7908C20.6413 16.7983 22.8053 14.6337 22.8129 11.9731C22.8129 9.35762 20.6563 7.20057 18.0414 7.19306C15.389 7.18554 13.2174 9.32004 13.2099 11.9506Z" fill="url(#icon-earning-0)"></path>
                                                        <defs>
                                                            <linearGradient id="icon-earning-0" x1="6" y1="32.5" x2="40" y2="6.5" gradientUnits="userSpaceOnUse">
                                                                <stop stopColor="#ED66B2"></stop>
                                                                <stop offset="1" stopColor="#86469C"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                    </svg>
                                                </div>
                                                <h3>{item?.name}</h3>
                                                <p>{item?.reward_value} {item?.reward_type_label}</p>
                                                <button
                                                    type="button"
                                                    style={{ fontSize: '14px', fontWeight: '400px', marginTop: '10px', minWidth: '110px', padding: '5px 14px', backgroundImage: 'linear-gradient(97.99deg, #ed66b2 18.85%, #86469c 116.55%)', border: 'none', cursor: 'pointer', borderRadius: '36px', color: '#fff' }}
                                                    onClick={() => {
                                                        if (!isAuth) {
                                                            setSignIn(true)
                                                        }
                                                    }}
                                                >
                                                    <span>{renderTextAction(item?.type)}</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        }
                    </div>
                </div>
            </div>
            <LoginRequired
                popupVisible={isSignIn}
                setPopupVisible={setSignIn}
            />
        </div>
    )
}

export default LoyaltyPageEarning
