import { View, Text } from 'react-native'
import React from 'react'
import LoyaltyPageEarning from './LoyaltyPageEarning'
import LoyaltyPageHeader from './LoyaltyPageHeader'
import LoyaltyPageRedeem from './LoyaltyPageRedeem'
import LoyaltyPageReferral from './LoyaltyPageReferral'
import LoyaltyPageProfile from './LoyaltyPageProfile'
import LoyaltyPageTier from './LoyaltyPageTier'

const LoyaltyPage = ({ shopUrl, customId, lang, isAuth }) => {
  return (
    <View>
      <LoyaltyPageHeader
        lang={lang}
        isAuth={isAuth}
      />
      <LoyaltyPageEarning
        shopUrl={shopUrl}
        customId={customId}
        lang={lang}
        isAuth={isAuth}
      />
      <LoyaltyPageRedeem
        shopUrl={shopUrl}
        customId={customId}
        lang={lang}
        isAuth={isAuth}
      />
      <LoyaltyPageReferral
        shopUrl={shopUrl}
        customId={customId}
        lang={lang}
        isAuth={isAuth}
      />
      <LoyaltyPageTier
        shopUrl={shopUrl}
        customId={customId}
        lang={lang}
        isAuth={isAuth}
      />
    </View>
  )
}

export default LoyaltyPage