/* eslint-disable prettier/prettier */
import React, { useRef, useState } from 'react'
import { CopyIcon } from '../../BonLoyaltyWidget'

const RedeemPopup = ({ popupVisible, setPopupVisible, discountCode }) => {
    const inputRef = useRef(null)
    const [isLabel, setLabel] = useState(false)
    const [labelCopy, setLabelCopy] = useState('Copy to Clipboard')
    const handleSelectUrl = () => {
        inputRef.current.select()
    }
    return (
        <>
            {popupVisible &&
                <div style={{ background: 'rgba(0,0,0,0.3)', height: '100vh', width: '100vw', display: 'flex', alignItems: 'center', justifyContent: 'center', position: 'fixed', top: 0, right: 0, bottom: 0, left: 0, zIndex: 100 }}>
                    <div style={{ padding: '30px', backgroundColor: '#fff', borderRadius: '20px', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', position: 'relative', maxWidth: '455px' }}>
                        <svg
                            viewBox="0 0 36 36"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            style={{ height: '36px', width: '36px', position: 'absolute', cursor: 'pointer', top: '-16px', right: '-16px', zIndex: '1000' }}
                            onClick={() => { setPopupVisible(false) }}
                        >
                            <circle cx="18" cy="18" r="18" fill="#444444"></circle>
                            <path fillRule="evenodd" clipRule="evenodd" d="M12.293 23.7069C12.4883 23.9024 12.744 24 12.9999 24C13.2558 24 13.5118 23.9024 13.707 23.7069L18 19.4139L22.2929 23.7069C22.4882 23.9024 22.7441 24 23 24C23.256 24 23.5117 23.9024 23.7069 23.7069C24.0976 23.3164 24.0976 22.6837 23.7069 22.293L19.4139 18L23.7069 13.707C24.0976 13.3165 24.0974 12.6835 23.7069 12.293C23.3164 11.9023 22.6837 11.9023 22.2929 12.293L18 16.586L13.707 12.2931C13.3165 11.9023 12.6835 11.9025 12.293 12.2931C11.9023 12.6836 11.9023 13.3163 12.293 13.707L16.586 18L12.293 22.2929C11.9023 22.6834 11.9023 23.3164 12.293 23.7069Z" fill="white"></path>
                        </svg>
                        {
                            discountCode ? <>
                                <h3>Redeem points</h3>
                                <p style={{ textAlign: 'center' }}>Apply this code to your shopping cart. If you do not use this code now, you can always find it in<b> My rewards</b> tab anytime</p>
                                <div style={{ width: '70%', height: '48px', marginTop: '20px', borderRadius: '10px', border: '1px solid #ed66b2', backgroundColor: '#fff', display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: '0 16px' }}>
                                    <input
                                        ref={inputRef}
                                        type="text"
                                        onChange={() => { }}
                                        defaultValue={discountCode}
                                        style={{ border: 'none', outline: 'none', fontWeight: '500', fontSize: '14px', lineHeight: '20px', opacity: '1', width: '80%', margin: 0, padding: 0, background: 'transparent' }}
                                    />
                                    <div style={{ position: 'relative' }}>
                                        <div
                                            onClick={() => {
                                                navigator.clipboard.writeText(discountCode)
                                                handleSelectUrl()
                                            }}
                                            onMouseOver={() => {
                                                setLabel(true)
                                            }}
                                            onMouseLeave={() => {
                                                setLabel(false)
                                                setLabelCopy('Copy to Clipboard')
                                            }}
                                            onMouseDown={() => {
                                                setLabelCopy('Copied')
                                            }}
                                        >
                                            <CopyIcon
                                                startColor={'#ed66b2'}
                                                endColor={'#86469c'}
                                            />
                                        </div>
                                        {isLabel && <div id='copyText' style={{ padding: '10px 0', width: '100px', background: 'rgba(0,0,0,0.7)', textAlign: 'center', display: 'flex', justifyContent: 'center', alignItems: 'center', color: '#fff', position: 'absolute', bottom: '30px', left: '-40px', borderRadius: '8px 0' }}>
                                            <p style={{ fontSize: '14px' }}>{labelCopy}</p>
                                        </div>}
                                    </div>
                                </div>
                                <button
                                    style={{ textTransform: 'uppercase', marginTop: '30px', fontSize: '16px', fontWeight: '600', width: '152px', borderRadius: '36px', height: '48px', color: '#ffffff', border: 'none', backgroundImage: 'linear-gradient(97.99deg, #ed66b2 18.85%, #86469c 116.55%)', cursor: 'pointer' }}
                                >Apply now</button>
                            </> : <p style={{ textAlign: 'center' }}>Not enough point</p>

                        }
                    </div>
                </div>
            }
        </>
    )
}

export default RedeemPopup
