/* eslint-disable prettier/prettier */
import React, { useEffect, useState, useRef } from 'react'
import { BASE_URL, SHOP_DATA, LoadingSpinner, REFERRAL, CopyIcon } from '../BonLoyaltyWidget'

const LoyaltyPageReferral = ({ shopUrl, lang, isAuth, customId }) => {
    const [isLoading, setLoading] = useState(true);
    const [loginRequired, setLoginRequired] = useState(false)
    const [isReferralUrl, setReferralUrl] = useState(false)
    const [customUrl, setCustomUrl] = useState('')
    const [listReferral, setListReferral] = useState([]);
    const inputRef = useRef(null)
    const [isLabel, setLabel] = useState(false)
    const [labelCopy, setLabelCopy] = useState('Copy to Clipboard')
    const [shopData, setShopData] = useState({})
    useEffect(() => {
        getListReferral()
        getInfoUser()
        // getShopData()
    }, [])
    const getShopData = async () => {
        setLoading(true)
        try {
            const response = await fetch(`${BASE_URL}${SHOP_DATA.GET_ALL}?shop=${shopUrl}&lang=${lang}`);
            const json = await response.json();
            setLoading(false)
            console.log(json)
            setShopData(json.data)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getShopData errrrrrrr')
        }
    }
    const getListReferral = async () => {
        try {
            setLoading(true)
             const response = await fetch(
                `${BASE_URL}${REFERRAL.GET_ALL}?cid=${customId}&shop=${shopUrl}`,
            );
            const json = await response.json();
            setLoading(false)
            setListReferral(json.data)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getListReferral error')
        }
    }
    const getInfoUser = () => {
        const user = localStorage.getItem('userInfo')
        setCustomUrl(`https://${window.location.host}?bon_ref=${user?.user?.referral_code}`)
    }
    const handleSelectUrl = () => {
        inputRef.current.select()
    }
    return (
        <div style={{ padding: '0px 50px' }}>
            <div style={{ opacity: 1.075, backgroundColor: '#ED66B20F' }} >
                <div>
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <div style={{ width: '70%' }}>
                            <h1
                                style={{ color: '#202528', fontWeight: '600', fontSize: '30px', lineHeight: 1.2, textAlign: 'center', marginBottom: 0 }}
                            >Referral Program</h1>
                            <p
                                className="caption-size"
                                style={{ textAlign: 'center', color: '#828282', lineHeight: 1.5, fontWeight: '400', fontSize: '20px' }}
                            >
                                Join or sign in to get your referral link and share with friends to earn rewards for each successful referral.
                            </p>
                        </div>
                    </div>
                    <div style={{ padding: '35px 50px', position: 'relative' }} >
                        {isLoading ? <LoadingSpinner color={'#ed66b2'} /> : <div style={{ width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <div style={{ width: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', flexWrap: 'wrap' }}>
                                {listReferral?.length > 0 && listReferral.map((item, index) => (
                                    <div key={index} style={{ marginBottom: '24px', minWidth: '242px', width: '40%' }}>
                                        <div style={{ height: "100%", borderRadius: '10px', border: '2px solid #86469c0f', backgroundColor: '#fff' }}>
                                            <div style={{ height: "100%", display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '48px 0' }} >
                                                <div style={{ height: '36px', width: '36px' }}>
                                                    <svg className="bon-icon" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M35.963 23.5588C35.3686 20.2197 33.1472 17.5387 30.218 16.2952C31.5179 15.2078 32.3482 13.5613 32.3482 11.7219C32.3482 8.45485 29.7302 5.79683 26.512 5.79683C23.2938 5.79683 20.6758 8.45457 20.6758 11.7219C20.6758 13.5624 21.5069 15.2095 22.8082 16.2971C22.3226 16.5037 21.8521 16.7502 21.4028 17.0376C20.2704 17.762 19.3036 18.7159 18.5584 19.8332C17.8001 19.3307 16.9846 18.9127 16.125 18.5918C17.9726 17.2595 19.1805 15.0689 19.1805 12.5969C19.1805 8.5458 15.9366 5.25 11.9491 5.25C7.96157 5.25 4.71758 8.5458 4.71758 12.5969C4.71758 15.0689 5.92553 17.2595 7.77316 18.5918C3.84115 20.0592 0.825401 23.5487 0.0431742 27.942C-0.101296 28.7543 0.120628 29.5828 0.651542 30.2151C1.17751 30.8411 1.94491 31.2 2.75707 31.2H21.1411C21.9532 31.2 22.7206 30.8411 23.2466 30.2151C23.7778 29.5828 23.9994 28.7543 23.8549 27.942C23.7585 27.4001 23.6278 26.8722 23.466 26.3601H33.6273C34.3266 26.3601 34.9874 26.0511 35.44 25.5125C35.8965 24.9692 36.0871 24.2569 35.963 23.5588ZM22.7852 11.7219C22.7852 9.61482 24.457 7.90089 26.512 7.90089C28.567 7.90089 30.2388 9.61482 30.2388 11.7219C30.2388 13.8287 28.567 15.5426 26.512 15.5426C24.457 15.5426 22.7852 13.8287 22.7852 11.7219ZM6.82696 12.5969C6.82696 9.70605 9.12475 7.35405 11.9491 7.35405C14.7734 7.35405 17.0714 9.70605 17.0714 12.5969C17.0714 15.4878 14.7734 17.8398 11.9491 17.8398C9.12475 17.8398 6.82696 15.4878 6.82696 12.5969ZM21.6299 28.8636C21.5569 28.9505 21.3965 29.0959 21.1411 29.0959H2.75707C2.50164 29.0959 2.34124 28.9505 2.26818 28.8636C2.19238 28.7732 2.07207 28.5809 2.12014 28.3097C2.98312 23.4624 7.11672 19.9439 11.9491 19.9439C16.7814 19.9439 20.9153 23.4624 21.778 28.3097C21.8263 28.5809 21.706 28.7732 21.6299 28.8636ZM33.8234 24.161C33.7935 24.1966 33.7284 24.2561 33.6276 24.2561H22.5794C21.9554 23.0898 21.154 22.0435 20.2138 21.1509C21.5876 18.971 23.9357 17.6467 26.512 17.6467C30.1372 17.6467 33.2381 20.2877 33.886 23.9268C33.9063 24.0416 33.8555 24.1229 33.8234 24.161Z" fill="url(#icon-referral-3)"></path>
                                                        <defs>
                                                            <linearGradient id="icon-referral-3" x1="7.6058e-07" y1="31" x2="34" y2="9" gradientUnits="userSpaceOnUse">
                                                                <stop stopColor="#ED66B2"></stop>
                                                                <stop offset="1" stopColor="#86469C"></stop>
                                                            </linearGradient>
                                                        </defs>
                                                    </svg>
                                                </div>
                                                <p style={{ color: '#202528', fontWeight: '600', fontSize: '15px', marginTop: '24px', padding: '0 20px', textAlign: 'center' }}>You will get {item?.trans_title_json?.filter(item => item?.lang === lang)[0].value}</p>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            {isAuth && <div style={{ width: '70%', height: '48px', marginTop: '20px', borderRadius: '10px', border: '1px solid #ed66b2', backgroundColor: '#fff', display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: '0 16px' }}>
                                <input
                                    ref={inputRef}
                                    type="text"
                                    onChange={() => { }}
                                    value={customUrl}
                                    style={{ border: 'none', outline: 'none', fontWeight: '500', fontSize: '14px', lineHeight: '20px', opacity: '1', width: '80%', margin: 0, padding: 0, background: 'transparent' }}
                                />
                                <div style={{ position: 'relative' }}>
                                    <div
                                        onClick={() => {
                                            navigator.clipboard.writeText(customUrl)
                                            handleSelectUrl()
                                        }}
                                        onMouseOver={() => {
                                            setLabel(true)
                                        }}
                                        onMouseLeave={() => {
                                            setLabel(false)
                                            setLabelCopy('Copy to Clipboard')
                                        }}
                                        onMouseDown={() => {
                                            setLabelCopy('Copied')
                                        }}
                                    >
                                        <CopyIcon
                                            startColor={'#ed66b2'}
                                            endColor={'#86469c'}
                                        />
                                    </div>
                                    {isLabel && <div id='copyText' style={{ padding: '10px 0', width: '100px', background: 'rgba(0,0,0,0.7)', textAlign: 'center', display: 'flex', justifyContent: 'center', alignItems: 'center', color: '#fff', position: 'absolute', bottom: '30px', left: '-40px', borderRadius: '8px 0' }}>
                                        <p style={{ fontSize: '14px' }}>{labelCopy}</p>
                                    </div>}
                                </div>
                            </div>}
                        </div>}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoyaltyPageReferral
