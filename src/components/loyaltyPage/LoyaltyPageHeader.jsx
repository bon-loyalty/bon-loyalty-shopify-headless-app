/* eslint-disable prettier/prettier */
import React from 'react'

const LoyaltyPageHeader = ({ isAuth, lang }) => {
    // const getCurrentDimension = () => {
    //     return {
    //         width: window.innerWidth,
    //         height: window.innerHeight
    //     }
    // }
    // const dimension = getCurrentDimension()
    // console.log(dimension, 'dimensionnnnnnnnnnnnnnnnnn');
    return (
        <div
            className='page-width'
            style={{ padding: '0px 50px' }}
        >
            <div
                className='bon-header'
                style={{ position: 'relative', padding: '150px 70px', maxHeight: '510px' }}
            >
                <img
                    style={{ position: 'absolute', zIndex: -10, right: 0, left: 0, bottom: 0, top: 0, width: '100%', height: '100%' }}
                    src='https://d31wum4217462x.cloudfront.net/img/banner_desktop.png'
                    // src={dimension.width >= 480 ? 'https://d31wum4217462x.cloudfront.net/img/banner_desktop.png' : 'https://d31wum4217462x.cloudfront.net/img/banner_mobile.png'}
                    alt=''
                    className='background'
                />
                <div>
                    <div
                        style={{ maxWidth: '470px' }}
                        className="content"
                    >
                        <h1
                            className="header-size"
                            style={{ color: '#ffffff', marginBottom: '8px', fontWeight: '500', lineHeight: 1.2 }}
                        >Rewards program</h1>
                        <p
                            className="caption-size"
                            style={{ color: '#ffffff', marginBottom: '16px', fontWeight: '400', lineHeight: 1.5 }}
                        >Become a member of our loyalty program and gain access to exclusive member benefits every time you shop. Many attractive rewards are waiting for you!</p>
                        {isAuth ?
                            <div>
                                <button
                                    style={{ textTransform: 'uppercase', margin: '0 12px 15px 0', fontSize: '16px', fontWeight: '600', minWidth: '152px', borderRadius: '10px', height: '45px', color: '#ffffff', border: 'none', backgroundImage: 'linear-gradient(97.99deg, #ed66b2 18.85%, #86469c 116.55%)', cursor: 'pointer' }}
                                >Earn Points</button>
                                <button
                                    style={{ textTransform: 'uppercase', margin: '0 12px 15px 0', fontSize: '16px', fontWeight: '600', minWidth: '152px', borderRadius: '10px', height: '45px', color: '#ed66b2', border: 'none', cursor: 'pointer' }}
                                >Redeem Points</button>
                            </div>
                            :
                            <div>
                                <button
                                    style={{ textTransform: 'uppercase', margin: '0 12px 15px 0', fontSize: '16px', fontWeight: '600', minWidth: '152px', borderRadius: '10px', height: '45px', color: '#ffffff', border: 'none', backgroundImage: 'linear-gradient(97.99deg, #ed66b2 18.85%, #86469c 116.55%)', cursor: 'pointer' }}
                                >Join now</button>
                                <button
                                    style={{ textTransform: 'uppercase', margin: '0 12px 15px 0', fontSize: '16px', fontWeight: '600', minWidth: '152px', borderRadius: '10px', height: '45px', color: '#ed66b2', border: 'none', cursor: 'pointer' }}
                                >Sign In</button>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoyaltyPageHeader
