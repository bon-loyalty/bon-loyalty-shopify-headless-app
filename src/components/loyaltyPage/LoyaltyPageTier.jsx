/* eslint-disable prettier/prettier */
import React, { useEffect, useState, useRef } from 'react'
import { BASE_URL, SHOP_DATA, LoadingSpinner, TIER } from '../BonLoyaltyWidget'

const LoyaltyPageTier = ({ shopUrl, lang, isAuth, customId }) => {
    const [isLoading, setLoading] = useState(true);
    const [vipTier, setVipTier] = useState([]);
    const [shopData, setShopData] = useState({})
    useEffect(() => {
        if (isAuth) {
            getVipTierAfterLogin()
        } else getVipTierBeforeLogin()
        // getShopData()
    }, [])
    const getShopData = async () => {
        setLoading(true)
        try {
            const response = await fetch(`${BASE_URL}${SHOP_DATA.GET_ALL}?shop=${shopUrl}&lang=${lang}`);
            const json = await response.json();
            setLoading(false)
            console.log(json)
            setShopData(json.data)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getShopData errrrrrrr')
        }
    }
    const getVipTierBeforeLogin = async () => {
        setLoading(true)
        try {
            const response = await fetch(
                `${BASE_URL}${TIER.GET_AFTER_LOGIN}?cid=${customId}&shop=${shopUrl}`,
            );
            const json = await response.json();
            setLoading(false)
            setVipTier(json.data)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getVipTier error')
        }
    }
    const getVipTierAfterLogin = async () => {
        setLoading(true)
        try {
            const response = await fetch(
                `${BASE_URL}${TIER.GET_AFTER_LOGIN}?cid=${customId}&shop=${shopUrl}`,
            );
            const json = await response.json();
            setLoading(false)
            setVipTier(json.data)
        } catch (error) {
            setLoading(false)
            console.log(error, 'getVipTier error')
        }
    }
    return (
        <div style={{ padding: '0px 50px' }}>
            <div style={{ opacity: 1.075, backgroundColor: '#ED66B20F' }} >
                <div >
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                        <div style={{ width: '70%' }}>
                            <h1
                                style={{ color: '#202528', fontWeight: '600', fontSize: '30px', lineHeight: 1.2, textAlign: 'center', marginBottom: 0 }}
                            >VIP Tiers</h1>
                            <p
                                className="caption-size"
                                style={{ textAlign: 'center', color: '#828282', lineHeight: 1.5, fontWeight: '400', fontSize: '20px' }}
                            >
                                Join our VIP loyalty program to unlock exclusive rewards available only to members.
                            </p>
                        </div>
                    </div>
                    <div style={{ padding: '35px 50px', position: 'relative' }} >
                        {isLoading ? <LoadingSpinner color={'#ed66b2'} /> :
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <div style={{ margin: '0 20px 24px 0',minWidth:'242px', width: '25%', position: 'relative' }}>
                                    {isAuth && <span style={{ background: '#86469c1a', borderRadius: '5px', color: '#86469c', padding: '2px 12px', position: 'absolute', top: '20px', right: '20px', fontSize: '16px', lineHeight: 1.5, fontWeight: '400' }}>Current tier</span>}
                                    <div style={{ height: "100%", borderRadius: '10px', border: '2px solid #86469c0f', backgroundColor: '#fff' }}>
                                        <div style={{ height: "100%", display: 'flex', flexDirection: 'column', alignItems: 'center', padding: '40px 16px 24px' }} >
                                            <div style={{ height: '36px', width: '36px', marginTop:'15px' }}>
                                                <svg className="bon-icon" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <g clipPath="url(#clip0_1345_7422)">
                                                        <path d="M26.2033 23.2982L22.732 16.3548C23.8682 14.6789 24.5328 12.6584 24.5328 10.4855C24.5328 4.70377 19.829 0 14.0473 0C8.26563 0 3.56186 4.70377 3.56186 10.4855C3.56186 12.6584 4.22644 14.6791 5.36292 16.3552L1.89111 23.2982C1.76401 23.5526 1.77768 23.8544 1.927 24.0963C2.07654 24.3381 2.34058 24.4855 2.62491 24.4855H6.60791L8.99771 27.6719C9.15366 27.8795 9.39719 28 9.65396 28C9.99426 28 10.2624 27.7971 10.3878 27.5465L13.6793 20.9637C13.8015 20.968 13.9241 20.9709 14.0473 20.9709C14.1706 20.9709 14.2932 20.968 14.4154 20.9637L17.7069 27.5465C17.8319 27.7966 18.1002 28 18.4407 28C18.6973 28 18.941 27.8795 19.0967 27.6719L21.4868 24.4855H25.4698C25.7541 24.4855 26.0181 24.3381 26.1675 24.0963C26.317 23.8544 26.3307 23.5526 26.2033 23.2982ZM9.51404 25.6256L7.67432 23.173C7.51944 22.9664 7.27634 22.8448 7.01807 22.8448H3.95215L6.49768 17.7542C7.94306 19.2547 9.82977 20.3273 11.947 20.7595L9.51404 25.6256ZM5.20249 10.4855C5.20249 5.60846 9.17032 1.64062 14.0473 1.64062C18.9243 1.64062 22.8922 5.60846 22.8922 10.4855C22.8922 15.3625 18.9243 19.3303 14.0473 19.3303C9.17032 19.3303 5.20249 15.3625 5.20249 10.4855ZM21.0764 22.8448C20.8183 22.8448 20.5752 22.9664 20.4201 23.173L18.5806 25.6256L16.1475 20.7595C18.2649 20.3271 20.1518 19.2547 21.5972 17.7538L24.1423 22.8446H21.0764V22.8448Z" fill="#DF9D7E"></path>
                                                        <path d="M18.2348 11.8462L20.5257 9.11179C20.7073 8.89517 20.7645 8.60037 20.6771 8.33163C20.5898 8.06268 20.3702 7.85782 20.0961 7.78925L16.6356 6.92365L14.7427 3.89981C14.5927 3.66013 14.33 3.51465 14.0474 3.51465C13.7647 3.51465 13.502 3.66013 13.352 3.89981L11.4595 6.92365L7.99927 7.78925C7.72498 7.85782 7.50537 8.06268 7.418 8.33142C7.33084 8.60037 7.38788 8.89517 7.56946 9.11179L9.86035 11.8462L9.61383 15.4045C9.59439 15.6865 9.72128 15.9586 9.94986 16.1248C10.297 16.3771 10.6501 16.2577 10.7388 16.222L14.0474 14.8886L17.3559 16.2222C17.6181 16.3278 17.9161 16.291 18.1446 16.125C18.3734 15.9588 18.5003 15.6867 18.4809 15.4047L18.2348 11.8462ZM16.7649 11.0459C16.6286 11.2087 16.5606 11.4176 16.5754 11.6293L16.7537 14.2105L14.3541 13.2433C14.0882 13.136 13.8549 13.1971 13.7408 13.2433L11.3412 14.2105L11.52 11.6295C11.5345 11.4178 11.4666 11.2087 11.3303 11.0459L9.66873 9.06287L12.1786 8.43503C12.3845 8.38354 12.5623 8.2543 12.6748 8.07443L14.0476 5.88116L15.4205 8.07443C15.5331 8.2543 15.7108 8.38354 15.9168 8.43503L18.4266 9.06287L16.7649 11.0459Z" fill="#DF9D7E"></path>
                                                    </g>
                                                    <defs>
                                                        <clipPath id="clip0_1345_7422">
                                                            <rect width="28" height="28" fill="white"></rect>
                                                        </clipPath>
                                                    </defs>
                                                </svg>
                                            </div>
                                            <h3 style={{ color: '#202528', fontWeight: '600', fontSize: '15px', textAlign: 'center', marginTop: '24px' }}>Entry Tier</h3>
                                            <p style={{ color: '#828282', fontWeight: '400', fontSize: '15px', textAlign: 'center', lineHeight: 1.2 }}>Earn 0 points</p>
                                            {
                                                isAuth && <div>
                                                    <p style={{ lineHeight: 1.5, margin: '8px 0 16px', textAlign: 'center', fontSize: '16px', fontWeight: '400', color: '#212529' }}>You have reached the highest tier!</p>
                                                    <div style={{ width: '100%', height: '16px', backgroundColor: '#ED66B2', borderRadius: '20px' }}></div>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoyaltyPageTier
